﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess
{
    public class Square
    {
        public const string Files = "ABCDEFGH";

        public int Rank { get; set; }
        public int File { get; set; }

        public Piece Piece { get; set; }

        public Square(int rank, int file)
        {
            Rank = rank;
            File = file;
            Validate();
        }

        public override string ToString()
        {
            return $"{Files[File]}{8 - Rank}";
        }

        /// <summary>
        /// Parses human-entered rank-by-file chess notation
        /// into machine-understood coordinates.
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public static Square Parse(string coordinate)
        {
            if (String.IsNullOrWhiteSpace(coordinate) || coordinate.Length != 2)
                throw new Exception("Invalid chessboard coordinate.");
            var f = coordinate.Substring(0, 1).ToUpperInvariant();
            var r = Int32.Parse(coordinate.Substring(1));
            return Parse(f, r);
        }

        /// <summary>
        /// Parses human-entered rank-by-file chess notation
        /// into machine-understood coordinates.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        public static Square Parse(string file, int rank)
        {
            if (String.IsNullOrWhiteSpace(file) || file.Length > 1)
                throw new Exception($"Invalid file: {file}");
            var f = Files.IndexOf(file);
            var r = 8 - rank;
            return new Square(r, f);
        }

        private void Validate()
        {
            if (Rank < 0 || Rank > 7)
                throw new Exception("Invalid rank.");
            if (File < 0 || File > 7)
                throw new Exception("Invalid file.");
        }
    }
}
