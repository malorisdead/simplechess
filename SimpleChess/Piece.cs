﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess
{
    public abstract class Piece
    {
        public abstract string Symbol { get; }
        public Color Color { get; set; }
        public abstract bool ValidateMove(Move move, Chessboard board);
        public abstract IEnumerable<Square> Threaten(Chessboard board, Square position);
    }
}
