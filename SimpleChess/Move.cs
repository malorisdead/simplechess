﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess
{
    public class Move
    {
        public Square From { get; set; }
        public Square To { get; set; }
        public bool Castle { get; set; }
        public bool Promotion { get; set; }
        public bool Check { get; set; }
        public bool Mate { get; set; }
        public Player Player { get; set; }
        public Piece Piece { get; set; }
        public Piece Capture { get; set; }
    }
}
