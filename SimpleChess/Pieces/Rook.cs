﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess.Pieces
{
    public class Rook : Piece
    {
        public override string Symbol => "R";

        public override bool ValidateMove(Move move, Chessboard board)
        {
            // Rooks can only move in one dimension, not both
            if (move.To.Rank == move.From.Rank)
                return true;
            if (move.To.File == move.From.File)
                return true;
            return false;
        }

        public override IEnumerable<Square> Threaten(Chessboard board, Square position)
        {
            yield break;
        }
    }
}
