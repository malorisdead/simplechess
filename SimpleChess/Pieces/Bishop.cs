﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess.Pieces
{
    public class Bishop : Piece
    {
        public override string Symbol => "B";

        public override bool ValidateMove(Move move, Chessboard board)
        {
            // Diagonal movement is easy.
            if (move.To.Rank - move.From.Rank == move.To.File - move.From.File)
                return true;
            return false;
        }

        public override IEnumerable<Square> Threaten(Chessboard board, Square position)
        {
            yield break;
        }
    }
}
