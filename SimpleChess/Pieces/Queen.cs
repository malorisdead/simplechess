﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess.Pieces
{
    public class Queen : Piece
    {
        public override string Symbol => "Q";

        public override bool ValidateMove(Move move, Chessboard board)
        {
            // Queens can move diagonally...
            if (Math.Abs(move.To.Rank - move.From.Rank) == Math.Abs(move.To.File - move.From.File))
                return true;
            // ... or horizontally
            if (move.To.Rank == move.From.Rank)
                return true;
            if (move.To.File == move.From.File)
                return true;
            return false;
        }

        public override IEnumerable<Square> Threaten(Chessboard board, Square position)
        {
            yield break;
        }
    }
}
