﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess.Pieces
{
    public class King : Piece
    {
        public override string Symbol => "K";

        public override bool ValidateMove(Move move, Chessboard board)
        {
            // TODO: Castling D:
            if (Math.Abs(move.To.Rank - move.From.Rank) > 1 || Math.Abs(move.To.File - move.From.File) > 1)
                return false;
            return true;
        }

        public override IEnumerable<Square> Threaten(Chessboard board, Square position)
        {
            yield break;
        }
    }
}
