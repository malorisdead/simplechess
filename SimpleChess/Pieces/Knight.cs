﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess.Pieces
{
    public class Knight : Piece
    {
        public override string Symbol => "N";

        public override bool ValidateMove(Move move, Chessboard board)
        {
            // Two in one dimension, one in the other
            if (move.To.Rank != move.From.Rank - 1 && move.To.Rank != move.From.Rank + 1 && move.To.Rank != move.From.Rank - 2 && move.To.Rank != move.From.Rank + 2)
                return false;
            if (move.To.File != move.From.File - 1 && move.To.File != move.From.File + 1 && move.To.File != move.From.File - 2 && move.To.File != move.From.File + 2)
                return false;
            return true;
        }

        public override IEnumerable<Square> Threaten(Chessboard board, Square position)
        {
            yield break;
        }
    }
}
