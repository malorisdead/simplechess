﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess.Pieces
{
    public class Pawn : Piece
    {
        public override string Symbol => "P";

        public override bool ValidateMove(Move move, Chessboard board)
        {
            // Good God pawn logic is annoying
            if (Color == Color.Black && move.To.Rank < move.From.Rank)
                return false;
            if (Color == Color.White && move.To.Rank > move.From.Rank)
                return false;

            var capture = board.GetPiece(move.To);
            if (move.To.File != move.From.File && capture == null)
                return false;
            if (move.To.File == move.From.File && capture != null)
                return false;

            var startRank = Color == Color.Black ? 1 : 6;
            var distance = Math.Abs(move.To.Rank - move.From.Rank);
            if (move.From.Rank == startRank && distance > 2)
                return false;
            if (move.From.Rank != startRank && distance != 1)
                return false;

            if (Math.Abs(move.To.File - move.From.File) > 1)
                return false;

            return true;
        }

        public override IEnumerable<Square> Threaten(Chessboard board, Square position)
        {
            yield break;
        }
    }
}
