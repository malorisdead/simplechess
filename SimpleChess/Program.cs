﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess
{
    class Program
    {
        static void Main(string[] args)
        {
            var player1 = new HumanPlayer { Color = Color.White };
            var player2 = new HumanPlayer { Color = Color.Black };

            var game = new Game(player1, player2);
            game.Play();

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("Done.");
            Console.ReadKey();
        }
    }
}
