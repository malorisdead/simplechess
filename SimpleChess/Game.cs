﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess
{
    public class Game
    {
        public Player White { get; set; }
        public Player Black { get; set; }
        public Chessboard Board { get; } = new Chessboard();

        public Game(Player white, Player black)
        {
            White = white;
            Black = black;
        }

        public void Play()
        {

            while (true)
            {
                bool valid;

                Board.Draw();

                do
                {
                    var m = White.Move();
                    valid = Board.Move(m);
                    if (!valid)
                        Invalid();
                } while (!valid);

                Board.Draw();

                do
                {
                    var m = Black.Move();
                    valid = Board.Move(m);
                    if (!valid)
                        Invalid();
                } while (!valid);
            }
        }

        protected void Invalid()
        {
            Console.WriteLine();
            Console.WriteLine("Invalid move.  Please try again.");
            Console.WriteLine();
        }
    }
}
