﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess
{
    public class HumanPlayer : Player
    {
        public override Move Move()
        {
            while (true)
            {
                Console.ForegroundColor = Color == Color.Black ? ConsoleColor.DarkRed : ConsoleColor.White;
                Console.WriteLine($"{Color} Player");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Please enter your move in the form [file][rank]:[file][rank] (ex. A2:A4)");
                Console.Write("    ");
                var entry = Console.ReadLine();
                if (!String.IsNullOrWhiteSpace(entry))
                {
                    var p = entry.Split(':');
                    try
                    {
                        var source = Square.Parse(p[0]);
                        var destination = Square.Parse(p[1]);

                        return new Move
                        {
                            From = source,
                            To = destination,
                            Player = this,
                        };
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine();
                    }
                }
            }
        }
    }
}
