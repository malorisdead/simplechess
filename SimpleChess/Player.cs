﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChess
{
    public abstract class Player
    {
        public Color Color { get; set; }
        public abstract Move Move();
    }
}
