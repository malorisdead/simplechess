﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleChess.Pieces;

namespace SimpleChess
{
    public class Chessboard
    {
        private readonly Square[,] _state = new Square[8, 8]; // Rank by file (row by column/number by letter - note that rank must be inverted for display and parse)

        public Chessboard()
        {
            // Set the initial state of the board with BRUTE FORCE BABY
            _state[0, 0] = new Square(0, 0) { Piece = new Rook   { Color = Color.Black } };
            _state[0, 1] = new Square(0, 1) { Piece = new Knight { Color = Color.Black } };
            _state[0, 2] = new Square(0, 2) { Piece = new Bishop { Color = Color.Black } };
            _state[0, 3] = new Square(0, 3) { Piece = new Queen  { Color = Color.Black } };
            _state[0, 4] = new Square(0, 4) { Piece = new King   { Color = Color.Black } };
            _state[0, 5] = new Square(0, 5) { Piece = new Bishop { Color = Color.Black } };
            _state[0, 6] = new Square(0, 6) { Piece = new Knight { Color = Color.Black } };
            _state[0, 7] = new Square(0, 7) { Piece = new Rook   { Color = Color.Black } };
            for (var i = 0; i < 8; i++)
            {
                _state[1, i] = new Square(1, i) {Piece = new Pawn {Color = Color.Black} };
            }

            for (var i = 2; i < 6; i++)
            {
                for (var j = 0; j < 8; j++)
                {
                    _state[i, j] = new Square(i, j);
                }
            }

            for (var i = 0; i < 8; i++)
            {
                _state[6, i] = new Square(6, i) { Piece = new Pawn { Color = Color.White } };
            }
            _state[7, 0] = new Square(7, 0) { Piece = new Rook   { Color = Color.White } };
            _state[7, 1] = new Square(7, 1) { Piece = new Knight { Color = Color.White } };
            _state[7, 2] = new Square(7, 2) { Piece = new Bishop { Color = Color.White } };
            _state[7, 3] = new Square(7, 3) { Piece = new Queen  { Color = Color.White } };
            _state[7, 4] = new Square(7, 4) { Piece = new King   { Color = Color.White } };
            _state[7, 5] = new Square(7, 5) { Piece = new Bishop { Color = Color.White } };
            _state[7, 6] = new Square(7, 6) { Piece = new Knight { Color = Color.White } };
            _state[7, 7] = new Square(7, 7) { Piece = new Rook   { Color = Color.White } };
        }

        public bool Move(Move move)
        {
            // Extremely simple validation
            if (move.From.Rank == move.To.Rank && move.From.File == move.To.File)
                return false;
            if (move.From.Rank < 0 || move.To.Rank < 0 || move.From.File < 0 || move.To.File < 0)
                return false;
            if (move.From.Rank > 7 || move.To.Rank > 7 || move.From.File > 7 || move.To.File > 7)
                return false;

            var from = _state[move.From.Rank, move.From.File];
            var piece = from.Piece;
            if (piece == null)
                return false;
            if (piece.Color != move.Player.Color)
                return false;

            if (!piece.ValidateMove(move, this))
                return false;

            // Hilariously basic movement.
            from.Piece = null;
            var to = _state[move.To.Rank, move.To.File];
            if (to.Piece != null)
            {
                if (to.Piece.Color == move.Player.Color)
                    return false;
                move.Capture = to.Piece;
            }
            to.Piece = piece;
            return true;
        }

        public Piece GetPiece(Square square)
        {
            return GetPiece(square.Rank, square.File);
        }

        public Piece GetPiece(int rank, int file)
        {
            return _state[rank, file]?.Piece;
        }

        public void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            Console.WriteLine();
            Console.WriteLine("     A  B  C  D  E  F  G  H");
            Console.WriteLine();
            for (var r = 0; r < 8; r++)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write($" {8 - r}  ");
                for (var f = 0; f < 8; f++)
                {
                    var piece = _state[r, f].Piece;
                    var s = piece?.Symbol ?? " ";
                    var bg = (r + f) % 2 == 1 ? ConsoleColor.Black : ConsoleColor.DarkGray;
                    var fg = piece?.Color == Color.Black ? ConsoleColor.DarkRed : ConsoleColor.White;
                    Console.BackgroundColor = bg;
                    Console.ForegroundColor = fg;
                    Console.Write($" {s} ");
                }
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write($"  {8 - r} ");
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("     A  B  C  D  E  F  G  H");
            Console.WriteLine();
        }
    }
}
